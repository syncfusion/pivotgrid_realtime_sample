﻿using System.Windows.Interactivity;
using System.Windows.Controls;

namespace TickingPivot.Action
{
    public class ButtonClickAction : TargetedTriggerAction<Button>
    {
        protected override void Invoke(object parameter)
        {
            if (this.AssociatedObject is Button)
            {
                (this.AssociatedObject as Button).Visibility = System.Windows.Visibility.Hidden;
            }
            this.Target.Visibility = System.Windows.Visibility.Visible;
        }
    }
}
