﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.IO;

namespace TickingPivot.Model
{
    public class VoteRecords : ObservableCollection<VoteRecord>
    {
        public VoteRecords()
        {
            InitializeVote();
        }

        public Candidates candidates = null;
        public States states = null;

        private static Random r = new Random(12);//(12123);

        public static VoteRecords GetFreshVotes()
        {
            return new VoteRecords();
        }

        public void InitializeVote()
        {
            this.states = new States(); //self initialized...
            this.candidates = new Candidates(new string[] { "Obama", "Romney" });
            foreach (State s in states)
            {
                foreach (string c in candidates)
                {
                    this.Add(new VoteRecord() { Candidate = c, State = s.Name, Votes = 0 });
                }
            }
        }

        public void AddAVoteRecord()
        {
            string name = "";
            this.Add(new VoteRecord() { Candidate = candidates[r.Next(candidates.Count)], State = name = this.states[r.Next(this.states.Count)].Name, Votes = /* r.Next(10000) + 1000 */  this.states.GetVotes(name) });
        }
    }


    public class VoteRecord : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private string state = string.Empty;
        public string State
        {
            get { return state; }
            set { OnPropertyChanging("State"); state = value; OnPropertyChanged("State"); }
        }

        private string candidate = string.Empty;
        public string Candidate
        {
            get { return candidate; }
            set { OnPropertyChanging("Candidate"); candidate = value; OnPropertyChanged("Candidate"); }
        }

        private int votes = 0;
        public int Votes
        {
            get { return votes; }
            set { OnPropertyChanging("Votes"); votes = value; OnPropertyChanged("Votes"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        protected virtual void OnPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }
    }

    public class Candidates : List<string>
    {
        public Candidates(IEnumerable<string> list) : base(list)
        {
        } 
    
        public Candidates()
        {
            this.Add("Obama");
            this.Add("Romney");
        }
    }


    public class States : List<State>
    {
        Dictionary<string, double> weights = new Dictionary<string, double>();
        public States()
        {
            this.Add(new State() { Delegates = 9, Name = "Alabama", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 3, Name = "Alaska", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 11, Name = "Arizona", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 6, Name = "Arkansas", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 55, Name = "California", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 9, Name = "Colorado", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 7, Name = "Connecticut", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 3, Name = "D.C.", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 3, Name = "Delaware", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 29, Name = "Florida", UsesProportionalAllocation = false });

            this.Add(new State() { Delegates = 16, Name = "Georgia", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 4, Name = "Hawaii", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 4, Name = "Idaho", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 20, Name = "Illinois", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 11, Name = "Indiana", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 6, Name = "Iowa", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 6, Name = "Kansas", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 8, Name = "Kentucky", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 8, Name = "Lousiana", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 4, Name = "Maine", UsesProportionalAllocation = true });

            this.Add(new State() { Delegates = 10, Name = "Maryland", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 11, Name = "Massachusetts", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 16, Name = "Michigan", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 10, Name = "Minnesota", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 6, Name = "Mississippi", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 10, Name = "Missouri", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 3, Name = "Montana", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 5, Name = "Nebraska", UsesProportionalAllocation = true });
            this.Add(new State() { Delegates = 6, Name = "Nevada", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 4, Name = "New Hampshire", UsesProportionalAllocation = false });

            this.Add(new State() { Delegates = 14, Name = "New Jersey", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 5, Name = "New Mexico", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 29, Name = "New York", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 15, Name = "North Carolina", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 3, Name = "North Dakota", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 18, Name = "Ohio", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 7, Name = "Oklahoma", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 7, Name = "Oregon", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 20, Name = "Pennsylvania", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 4, Name = "Rhode Island", UsesProportionalAllocation = false });

            this.Add(new State() { Delegates = 9, Name = "South Carolina", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 3, Name = "South Dakota", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 11, Name = "Tennessee", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 38, Name = "Texas", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 6, Name = "Utah", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 3, Name = "Vermont", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 13, Name = "Virginia", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 12, Name = "Washington", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 5, Name = "West Virginia", UsesProportionalAllocation = false });
            this.Add(new State() { Delegates = 10, Name = "Wisconsin", UsesProportionalAllocation = false });

            this.Add(new State() { Delegates = 3, Name = "Wyoming", UsesProportionalAllocation = false });
            
            double w = this.Sum(p => p.Delegates);

            foreach (State s in this)
            {
                weights.Add(s.Name, s.Delegates / w);
            }
          
        }

        public int GetVotes(string stateName)
        {
              return (int) (weights[stateName] * 500000);
         }

        public State this[string stateName]
        {
            get
            {
                return this.Where(p => p.Name == stateName).FirstOrDefault();
            }
        }
    }

    public class State
    {
        public string Name { get; set; }
        public int Delegates { get; set; }
        public bool UsesProportionalAllocation { get; set; }
    }

   
}
