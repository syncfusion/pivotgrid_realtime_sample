﻿using System.Windows.Interactivity;
using Syncfusion.Windows.Controls.PivotGrid;
using System;

namespace TickingPivot.Behavior
{
    public class PivotGridLoadedBehavior : Behavior<PivotGridControl>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.Loaded += new System.Windows.RoutedEventHandler(AssociatedObject_Loaded);

            if (this.AssociatedObject.Name == "electoralCollegePivotGrid")
            {
                //subscribe to this event on the electoral grid to force the modified display settings on the internal gridcontrol.
                this.AssociatedObject.PivotEngine.PivotSchemaChanged += new Syncfusion.PivotAnalysis.Base.PivotSchemaChangedEventHandler(PivotEngine_PivotSchemaChanged);
            }
        }

        void PivotEngine_PivotSchemaChanged(object sender, Syncfusion.PivotAnalysis.Base.PivotSchemaChangedArgs e)
        {
            if (this.AssociatedObject.InternalGrid != null)
            {
                //use this event force the modified display settings on the internal gridcontrol.
                //use a BeginInvoke to postpone the update uuntil after the schemachange actions have been completed.
                this.AssociatedObject.InternalGrid.Dispatcher.BeginInvoke(new System.Action(() =>
                    {
                        //hides the column grand totals
                        this.AssociatedObject.InternalGrid.Model.ColumnCount -= 1;

                        //allows the bottom grand total to always be visible
                        this.AssociatedObject.InternalGrid.Model.FooterRows = 1;
                    }));
            }
        }

         

        void AssociatedObject_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {

            if (this.AssociatedObject.InternalGrid != null)
            {
                //hides the column grand totals
                this.AssociatedObject.InternalGrid.Model.ColumnCount -= 1;

                //allows the bottom grand total to always be visible
                this.AssociatedObject.InternalGrid.Model.FooterRows = 1;
            }
        }

        protected override void OnDetaching()
        {
            this.AssociatedObject.Loaded -= new System.Windows.RoutedEventHandler(AssociatedObject_Loaded);
            if (this.AssociatedObject.Name == "electoralCollegePivotGrid")
            {
                this.AssociatedObject.PivotEngine.PivotSchemaChanged -= new Syncfusion.PivotAnalysis.Base.PivotSchemaChangedEventHandler(PivotEngine_PivotSchemaChanged);
            }
            base.OnDetaching();
        }
    }
}
