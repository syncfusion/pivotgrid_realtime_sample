﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using Syncfusion.Windows.Shared;
using TickingPivot.Model;
using Syncfusion.PivotAnalysis.Base;
using System.ComponentModel;

namespace TickingPivot.ViewModel
{
    public class ElectoralCollegeSupport : NotificationObject 
    {
        DispatcherTimer electoralTimer = null; 
        CancellationTokenSource cancelVote = null;
        public ElectoralCollegeSupport()
        {

        }

        private VoteRecords _recordList1;

        public VoteRecords PopularRecords
        {
            get
            {
                _recordList1 = _recordList1 ?? VoteRecords.GetFreshVotes();
                return _recordList1;
            }
            set
            {
                _recordList1 = value;
            }
        }

        private VoteRecords _recordList2;

        public VoteRecords ElectoralRecords
        {
            get
            {
                _recordList2 = _recordList2 ?? VoteRecords.GetFreshVotes();
                return _recordList2;
            }
            set
            {
                _recordList2 = value;
                RaisePropertyChanged("ElectoralRecords");
            }
        }



        private bool _canStartAction = true;
        private bool _canStopAction = true;
        
        private DelegateCommand _updateCommand;
        public DelegateCommand UpdateCommand
        {
            get
            {
                _updateCommand = new DelegateCommand(StartAction, CanStartAction);
                return _updateCommand;
            }
        }

        private void StartAction(object parm)
        {
            if (parm is PivotEngine)
            {
                //use a Task to update the vote count off the UI therad.
                this.cancelVote = new CancellationTokenSource();
                Task streamVotes = Task.Factory.StartNew(new System.Action(() =>
                {
                    while (true)
                    {
                        //add 2 new vote counts every 50 milliseconds
                        for (int i = 0; i < 2; ++i)
                        {
                            this.PopularRecords.AddAVoteRecord();
                        }

                        if (cancelVote.IsCancellationRequested)
                        {
                            break;
                        }
                        Thread.Sleep(50);
                    }
                }), cancelVote.Token);

                //set up timer to periodically repopulate electoral pivotcontrol.
                this.electoralTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(5) };
                this.electoralTimer.Tick += (s, e1) =>
                {
                    this.ElectoralRecords = GetElectoralCollegeResults(parm as PivotEngine);
                };
                this.electoralTimer.Start();
            }
        }

        private bool CanStartAction(object parm)
        {
            return _canStartAction;
        }

        private DelegateCommand _cancelCommand;

        public DelegateCommand CancelCommand
        {
            get
            {
                _cancelCommand = _cancelCommand = new DelegateCommand(StopAction, CanStopAction);
                return _cancelCommand;
            }
        }

        private void StopAction(object parm)
        {
            //end the streamVotes task
            cancelVote.Cancel();
        }

        private bool CanStopAction(object parm)
        {
            return _canStopAction;
        }

        private VoteRecords GetElectoralCollegeResults(PivotEngine engine)
        {
            VoteRecords data = new VoteRecords();

           lock (engine)
            {
                for (int i = 1; i < engine.RowCount - 2; ++i)
                {
                    //see which candidate has the most votes and give him all the electoral votes from that state
                    double d1 = engine[i, 1].DoubleValue;
                    double d2 = engine[i, 2].DoubleValue;
                    string candidate = (d1 > d2) ? data.candidates[0] : ((d1 < d2) ? data.candidates[1] : "");
                    if (candidate.Length > 0)
                    {
                        data.Add(new VoteRecord()
                                   {
                                       Candidate = candidate,
                                       State = data.states[engine[i, 0].FormattedText].Name,
                                       Votes = data.states[engine[i, 0].FormattedText].Delegates
                                   });
                    }
                }
            }

            return data;
        }
    }
}
